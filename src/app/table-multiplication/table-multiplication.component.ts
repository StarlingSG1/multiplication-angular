import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-table-multiplication',
  templateUrl: './table-multiplication.component.html',
  styleUrls: ['./table-multiplication.component.scss']
})
export class TableMultiplicationComponent implements OnInit {
  tab = [
    {num: 1},{num: 2},{num: 3},{num: 4},{num: 5},{num: 6},{num: 7},{num: 8},{num: 9},{num: 10}];
      
    
  @Input() chiffre!: number;

  isSubmitted = false;
 badChiffre = false;


  @Output() leChiffre = new EventEmitter<string>();


  identForm!: FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      tableau: new FormControl(''),

      });
  }

  get formControls() { return this.identForm.controls; }


  ident() {
    this.isSubmitted = true;
 console.log("ident :" + this.identForm.value.chiffre);
 if (this.identForm.value.chiffre == '') {
 this.badChiffre = true;
 return;
 } else {
 this.leChiffre.emit(this.identForm.value.chiffre);
 }

    }
  }