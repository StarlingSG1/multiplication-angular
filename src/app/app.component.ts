import { Component, Input, Output, EventEmitter, SystemJsNgModuleLoader } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  chiffre = 0;
  
  
  identForm!: FormGroup;
  isSubmitted = false;
  badChiffre = false;
  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      chiffre: new FormControl(''),
      });
    
    
  }
  
  get formControls() { return this.identForm.controls; }

    
  submit() {
    this.isSubmitted = true;
    this.chiffre = this.identForm.get('chiffre')?.value;
    console.log(this.chiffre);
    if(this.identForm.value.chiffre != '' && this.identForm.value.chiffre != null){
    this.chiffre = this.identForm.get('chiffre')?.value;
    }
    else{
        this.chiffre = 1;
    } 
}


}